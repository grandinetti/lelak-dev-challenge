# Lelak Dev Challenge - Leandro Grandinetti
*Stack:*
* Node.js
* Webpack
* Typescript
* React
* Redux

## Application requirements / User Stories
* As a user I need to write on the screen in a specific area that simulates a paper, so that I can preview what I will get when I print.
* As a user I need to have a small library of predefined elements to use on my document, so that I can create my document with more ease.
* As a user I need to be able to drag and drop elements from the library, like svgs or images, to the simulated paper, so that I can easily use the assets that I need.
* As a user I need to be able to reorder the elements from the library, so that they stay in the same position even when I refresh the screen, so that I can keep my library the way I need.
* As a user I need to be able to select an element on the simulated paper and see on a panel basic element properties like name, size and position.


## Running
* run **npm install** to download dependencies
* run **npm start** to build and start webpack-dev-server
* open **http://localhost:3000/**

## Build options
* **npm run build** for single build (creates files and index.html in build/ folder)
