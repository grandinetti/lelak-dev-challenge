import React from 'react';
import styles from './style.css';

export namespace ToolButton {
    export interface Props {
        label: string;
        style: string;
        onClick: (style: string) => void;

    }
}

export const ToolButton = ({label, style, onClick}: ToolButton.Props) : JSX.Element => {
    return (
        <button 
            onClick={() => onClick(style)} 
            className={styles.styleButton} 
            dangerouslySetInnerHTML={{__html: label}}
        />
    );
}