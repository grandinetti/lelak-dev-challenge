import React from 'react';
import styles from'./style.css';
import { RouteComponentProps } from 'react-router';
import 'draft-js/dist/Draft.css';
import { Editor, EditorState, RichUtils } from 'draft-js';
import { ToolButton } from 'app/components/ToolButton';
export namespace EditorApp {
    export interface Props extends RouteComponentProps<void> { }
}

export const EditorApp = ({ history, location }: EditorApp.Props) => {
    const [editorState, setEditorState] = React.useState(
        () => EditorState.createEmpty(),
    );

    const handleEditorState = (state: EditorState) => {
        console.log(state);
        setEditorState(state);
    };

    const handleKeyCommand = (command: string, editorState: EditorState) => {
        const newState = RichUtils.handleKeyCommand(editorState, command);

        if (newState) {
            setEditorState(newState);
            return 'handled';
        }

        return 'not-handled';
    }

    const toolSetButtons  = [
        {label: "<b>B</b>", style: "BOLD"},
        {label: "<i>I</i>", style: "ITALIC"},
        {label: "<u>U</u>", style: "UNDERLINE"}
    ];

    const ontoolSetButtonClick = (style: string) : void => {
        setEditorState(RichUtils.toggleInlineStyle(editorState, style));
    }


    return (
        <div className={styles['RichEditor-root']}>
            <ul>
                {toolSetButtons.map((btn)=> (
                    <ToolButton label={btn.label} style={btn.style} onClick={ontoolSetButtonClick}/>
                ))}
            </ul>
            <div className={styles['RichEditor-editor']}>
                <Editor
                    editorState={editorState}
                    onChange={handleEditorState}
                    handleKeyCommand={handleKeyCommand}
                />
            </div>
        </div>
    );
};