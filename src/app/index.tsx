import React from 'react';
import { Route, Switch } from 'react-router';
import { EditorApp } from 'app/containers/EditorApp';
import { hot } from 'react-hot-loader';

export const App = hot(module)(() => (
  <Switch>
    <Route path="/" component={EditorApp} />
  </Switch>
));
